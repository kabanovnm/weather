import Vue from 'vue'
import Vuex from 'vuex'
import api from '../helpers/api'
import { CITY_NAMES, DEFAULT_PARAMS } from '../constants'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    data: [],
    dataMainCard: {},
    searchError: false
  },
  getters: {
    getData: (state) => state.data,
    getDataMainCard: (state) => state.dataMainCard,
    getSearchError: (state) => state.searchError
  },
  mutations: {
    SET_DATA_MAIN_CARD: (state, payload) => {
      state.dataMainCard = {
        name: payload.name,
        location: payload.sys.country,
        weather: payload.weather[0].main,
        temperature: payload.main.temp,
        humidity: payload.main.humidity
      }
    },
    SET_SEARCH_ERROR: (state, payload) => {
      state.searchError = payload
    },
    SET_CITY: (state, payload) => {
      const indexCity = state.data.findIndex(item => item.id === payload.id)
      const cityWeather = {
        name: payload.name,
        location: payload.sys.country,
        weather: payload.weather[0].main,
        temperature: payload.main.temp,
        humidity: payload.main.humidity,
        id: payload.id
      }
      if (indexCity >= 0) {
        state.data.splice(indexCity, 1, cityWeather)
      } else {
        state.data.push(cityWeather)
      }
    },
    REMOVE_CITY: (state, cityId) => {
      const indexCity = state.data.findIndex(item => item.id === cityId)
      if (indexCity >= 0) {
        state.data.splice(indexCity, 1)
      }
    },
    UPDATE_LOCAL_STORAGE: (state) => {
      const cityNames = state.data.map(item => item.name)
      localStorage.setItem(CITY_NAMES, cityNames.join())
    }
  },
  actions: {
    async addCityByName ({ commit }, city) {
      try {
        const response = await api.get('/weather', {
          params: {
            ...DEFAULT_PARAMS,
            q: city
          }
        })
        commit('SET_SEARCH_ERROR', false)
        commit('SET_CITY', response.data)
        commit('UPDATE_LOCAL_STORAGE')
      } catch (e) {
        if (e.response.status === 404) {
          commit('SET_SEARCH_ERROR', true)
        }
      }
    },
    async getWeatherMainCard ({ commit }) {
      const response = await api.get('/weather', {
        params: {
          ...DEFAULT_PARAMS,
          q: 'moscow'
        }
      })

      commit('SET_DATA_MAIN_CARD', response.data)
    },
    async getWeather ({ commit }, city) {
      try {
        const response = await api.get('/weather', {
          params: {
            ...DEFAULT_PARAMS,
            q: city
          }
        })
        commit('SET_CITY', response.data)
      } catch (e) {
        console.log('error', e)
      }
    },
    removeCityCard ({ commit }, cityId) {
      commit('REMOVE_CITY', cityId)
      commit('UPDATE_LOCAL_STORAGE')
    },
    clearError ({ commit }) {
      commit('SET_SEARCH_ERROR', false)
    }
  }
})
