export const CITY_NAMES = 'CITY_NAMES'

export const DEFAULT_PARAMS = {
  units: 'metric',
  lang: 'en'
}
