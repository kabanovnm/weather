import axios from 'axios'

export default axios.create({
  baseURL: 'http://api.openweathermap.org/data/2.5',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  params: {
    APPID: process.env.VUE_APP_WEATHER_ID
  }
})
